<?php 

require 'functionRG.php';

//ambil data di url


$id = $_GET["id"];

//query data barang berdasarkan idnya

$user = query_fg("SELECT * FROM daftar WHERE id = $id")[0];

if( isset($_POST["submit"]) ) {

	//cek apakah data berhasil di ubah atau tidak

	if( ubah_user($_POST) > 0 ) {

		echo "
			<script>
				alert('data berhasil diubah!');
				document.location.href = 'kelola.php';
			</script>
		";
	} else {

		echo "
			<script>
				alert('data gagal diubah!');
				document.location.href = 'kelola.php';
			</script>

		";

	}

		
}

 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Ubah User</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- Custom Theme files -->
	<link href="daftar.css" rel="stylesheet" type="text/css" media="all" />
	<!-- //Custom Theme files -->
	<!-- web font -->
	<link href="//fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i" rel="stylesheet">
	<!-- //web font -->
</head>
<body>

	<!-- main -->
	<div class="main-w3layouts wrapper">
		<h1>UBAH USER </h1>
		<div class="main-agileinfo">
			<div class="agileits-top">
				<form action="" method="post">
					<input type="hidden" name="id" value="<?= $user["id"]; ?>">
					<input class="text" type="text" name="firstname" id="firstname" value="<?= $user["firstname"]; ?>" required>
					<input class="text" type="text" name="lastname" id="lastname" value="<?= $user["lastname"]; ?>" required>
					<input class="text" type="text" name="username" id="username" value="<?= $user["username"]; ?>" required>
					<input class="text" type="text" name="password" id="password" value="<?= $user["password"]; ?>" required>
					<input class="text email" type="text" name="email" id="email" value="<?= $user["email"]; ?>" required>
					<input class="text" type="text" name="city" id="city" value="<?= $user["city"]; ?>" required>
					<input type="submit" name="submit" value="UBAH">
				</form>
				<p> <a href="kelola.php">Lihat Daftar User</a></p>
			</div>
		</div>
		<!-- copyright -->
		<div class="colorlibcopy-agile">
			<p>Boya Hawks Studio</p>
		</div>
		<!-- //copyright -->
		<ul class="colorlib-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
	<!-- //main -->


</body>
</html>