<?php 

require 'functionNM.php';

if( isset($_POST["submit"]) ) {

	//cek apakah data berhasil di tambah atau tidak

	if( tambah_namabarang($_POST) > 0 ) {

		echo "
			<script>
				alert('data berhasil ditambahkan!');
				document.location.href = 'namabarang.php';
			</script>
		";
	} else {

		echo "
			<script>
				alert('data gagal ditambahkan!');
				document.location.href = 'namabarang.php';
			</script>

		";

	}

		
}

 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Tambah data barang</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- Custom Theme files -->
	<link href="tambah.css" rel="stylesheet" type="text/css" media="all" />
	<!-- //Custom Theme files -->
	<!-- web font -->
	<link href="//fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i" rel="stylesheet">
	<!-- //web font -->
</head>
<body>

	<!-- main -->
	<div class="main-w3layouts wrapper">
		<h1>Tambah Data Barang </h1>
		<div class="main-agileinfo">
			<div class="agileits-top">
				<form action="" method="post">
					<input class="text" type="text" name="kodebarang" id="kodebarang" placeholder="Kode Barang" required>
					<input class="text" type="text" name="namabarang" id="namabarang" placeholder="Nama Barang" required>
					<input class="text" type="text" name="hargabeli" id="hargabeli" placeholder="Harga Beli" required>
					<input class="text" type="text" name="hargajual" id="hargajual" placeholder="Harga Jual" required>
					<input class="text" type="text" name="stok" id="stok" placeholder="Stok" required>
					<input type="submit" name="submit" value="TAMBAH">
				</form>
				<p> <a href="namabarang.php">Lihat List Barang</a></p>
			</div>
		</div>
		<!-- copyright -->
		<div class="colorlibcopy-agile">
			<p>Boya Hawks Studio</p>
		</div>
		<!-- //copyright -->
		<ul class="colorlib-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
	<!-- //main -->


</body>
</html>