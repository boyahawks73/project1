<?php 

require 'functionNM.php';

//ambil data di url


$id = $_GET["id"];

//query data barang berdasarkan idnya

$barang = query_NM("SELECT * FROM namabarang WHERE id = $id")[0];

if( isset($_POST["submit"]) ) {

	//cek apakah data berhasil di ubah atau tidak

	if( ubah_namabarang($_POST) > 0 ) {

		echo "
			<script>
				alert('data berhasil diubah!');
				document.location.href = 'namabarang.php';
			</script>
		";
	} else {

		echo "
			<script>
				alert('data gagal diubah!');
				document.location.href = 'namabarang.php';
			</script>

		";

	}

		
}

 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Ubah data barang</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- Custom Theme files -->
	<link href="tambah.css" rel="stylesheet" type="text/css" media="all" />
	<!-- //Custom Theme files -->
	<!-- web font -->
	<link href="//fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i" rel="stylesheet">
	<!-- //web font -->
</head>
<body>

	<!-- main -->
	<div class="main-w3layouts wrapper">
		<h1>Ubah Data Barang </h1>
		<div class="main-agileinfo">
			<div class="agileits-top">
				<form action="" method="post">
					<input type="hidden" name="id" value="<?= $barang["id"]; ?>">
					<input class="text" type="text" name="kodebarang" id="kodebarang" value="<?= $barang["kodebarang"]; ?>" required>
					<input class="text" type="text" name="namabarang" id="namabarang" value="<?= $barang["namabarang"]; ?>" required>
					<input class="text" type="text" name="hargabeli" id="hargabeli" value="<?= $barang["hargabeli"]; ?>" required>
					<input class="text" type="text" name="hargajual" id="hargajual" value="<?= $barang["hargajual"]; ?>" required>
					<input class="text" type="text" name="stok" id="stok" value="<?= $barang["stok"]; ?>" required>
					<input type="submit" name="submit" value="UBAH">
				</form>
				<p> <a href="namabarang.php">Lihat List Barang</a></p>
			</div>
		</div>
		<!-- copyright -->
		<div class="colorlibcopy-agile">
			<p>Boya Hawks Studio</p>
		</div>
		<!-- //copyright -->
		<ul class="colorlib-bubbles">
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ul>
	</div>
	<!-- //main -->


</body>
</html>